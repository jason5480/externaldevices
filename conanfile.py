from conans import ConanFile, CMake


class ExternaldevicesConan(ConanFile):
    name = "externaldevices"
    version = "0.1"
    description = "External Devices library"
    author = "Iason Nikolas iason.nikolas@gmail.com"
    url = "https://bitbucket.org/jason5480/externaldevices/src/master"
    license = "https://bitbucket.org/jason5480/externaldevices/src/master/LICENSE.txt"
    exports = ["LICENSE"]
    exports_sources = "src/*"
    settings = "os", "compiler", "build_type", "arch"
    options = { "shared": [True, False] }
    default_options = (
        "shared=False"
    )
    build_requires = (
        "ninja_installer/1.9.0@bincrafters/stable"
    )
    generators = "cmake"

    def _configure_cmake(self):
        cmake = CMake(self, generator='Ninja')
        cmake.configure(source_folder="src")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        self.copy("LICENSE", dst="license", ignore_case=True, keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["externaldevices"]
