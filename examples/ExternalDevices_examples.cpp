// ExecutableSensor_examples.cpp : Defines the entry point for the console application.

#include "ExternalDevices/ExecutableDevice.hpp"
#include "ExternalDevices/MeasurableDevice.hpp"
#include <iostream>
#include <string>

int main()
{
	ext_executable_device executable;
	ext_measurable_device measurable;

	executable.execute(std::cout);
	measurable.measure(std::cout);

	std::string temp;
	std::cout << "Press Enter to Continue";
	std::getline(std::cin, temp);
	return 0;
}

