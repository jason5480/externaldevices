# ExternalDevices Examples
cmake_minimum_required(VERSION 3.18)
project(ExternalDevicesExamples LANGUAGES CXX)

# dependencies
if(NOT TARGET LLTB::ExternalDevices) # if not part of the project find the installed library
	find_package(ExternalDevices CONFIG REQUIRED)
endif()

# target definitions
add_executable(ExternalDevicesExamples ExternalDevices_examples.cpp)
target_link_libraries(ExternalDevicesExamples
	PRIVATE
		LLTB::ExternalDevices
)