# ExternalDevices Tests
cmake_minimum_required(VERSION 3.18)
project(ExternalDevicesGui LANGUAGES CXX)

include(${CMAKE_CURRENT_SOURCE_DIR}/../cmake/conan.cmake)

# Add remotes that contain the package dependencies
conan_add_remote(NAME jason5480 URL
				 https://api.bintray.com/conan/jason5480/public-conan)

conan_add_remote(NAME conan-center
                 URL https://api.bintray.com/conan/conan/conan-center)

conan_add_remote(NAME bincrafters URL
				 https://api.bintray.com/conan/bincrafters/public-conan)

# Make sure to use conanfile.py to define dependencies, to stay consistent
conan_cmake_run(CONANFILE conanfile.py
				BASIC_SETUP CMAKE_TARGETS) # NO_OUTPUT_DIRS BUILD missing

# dependencies
if(NOT TARGET LLTB::ExternalDevices) # if not part of the project find the installed library
	find_package(ExternalDevices CONFIG REQUIRED)
endif()

# Turn on automatic invocation of the MOC, UIC & RCC
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

# Find includes in the build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Make this a GUI application on Windows
if(WIN32)
  set(CMAKE_WIN32_EXECUTABLE ON)
endif()

# Find the QtWidgets library
find_package(Qt5 COMPONENTS Widgets REQUIRED)

# target definitions
add_executable(ExternalDevicesGui main.cpp mainwindow.cpp mainwindow.ui resources.qrc)
target_link_libraries(ExternalDevicesGui
	PRIVATE
		LLTB::ExternalDevices
		Qt5::Widgets
		CONAN_PKG::fmt
		CONAN_PKG::spdlog
)