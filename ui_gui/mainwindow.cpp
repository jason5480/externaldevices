#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

#include <sstream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::onExit);
    connect(ui->executePushButton, &QPushButton::clicked, this, &MainWindow::onExecute);

    try
    {
        auto logger = spdlog::basic_logger_mt("logger", "logs/log.txt");
        spdlog::register_logger(logger);
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onExit()
{
    this->close();
}

void MainWindow::onExecute()
{
    std::ostringstream out;
    device.execute(out);

    auto logger = spdlog::get("logger");
    logger->info("Output from device: \"{}\"", out.str());
    logger->flush();
}
