import os

from conans import ConanFile, CMake


class ExternaldevicesTests(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    
    requires = (
        "qt/5.15.2@bincrafters/stable",
        "fmt/6.2.1",
        "spdlog/1.5.0"
    )
    
    default_options = (
        "qt:shared=True"
    )

    def build(self):
        cmake = CMake(self, generator='Ninja')
        # Current dir is "tests/build/<build_id>" and CMakeLists.txt is
        # in "tests"
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dll", dst="bin/plugins/platforms", src="plugins/platforms")

    def test(self):
        cmake = CMake(self)
        self.run("ctest -VV -C %s" % cmake.build_type)
