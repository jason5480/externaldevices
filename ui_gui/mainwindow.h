#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ExternalDevices/ExecutableDevice.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onExit();
    void onExecute();

private:
    Ui::MainWindow *ui;

    ext_executable_device device;
};

#endif // MAINWINDOW_H
