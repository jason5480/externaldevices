import os

from conans import ConanFile, CMake


class ExternaldevicesTests(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    
    requires = (
        "gtest/1.10.0"
    )

    def build(self):
        cmake = CMake(self, generator='Ninja')
        # Current dir is "tests/build/<build_id>" and CMakeLists.txt is
        # in "tests"
        cmake.configure()
        cmake.build()

    def test(self):
        cmake = CMake(self)
        self.run("ctest -VV -C %s" % cmake.build_type)
