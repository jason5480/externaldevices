#include "gtest/gtest.h"
#include "ExternalDevices/ExecutableDevice.hpp"
#include "ExternalDevices/MeasurableDevice.hpp"
#include <sstream>

TEST(ExternalSensors, executable)
{
	ext_executable_device executable;

	std::ostringstream out;
	executable.execute(out);
	std::string intended = "Executable device triggered";
	ASSERT_STREQ(out.str().c_str(), intended.c_str());
}

TEST(ExternalSensors, measurable)
{
	ext_measurable_device measurable;

	std::ostringstream out;
	measurable.measure(out);
	std::string intended = "Measurable device triggered";
	ASSERT_STREQ(out.str().c_str(), intended.c_str());
}