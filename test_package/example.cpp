#include "ExternalDevices/ExecutableDevice.hpp"
#include "ExternalDevices/MeasurableDevice.hpp"
#include <iostream>
#include <string>

int main()
{
	ext_executable_device executable;
	ext_measurable_device measurable;

	executable.execute(std::cout);
	measurable.measure(std::cout);
	
	return 0;
}