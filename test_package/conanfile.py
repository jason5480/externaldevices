import os

from conans import ConanFile, CMake


class ExternaldevicesTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    
    build_requires = (
        "ninja_installer/1.9.0@bincrafters/stable"
    )

    def build(self):
        cmake = CMake(self, generator='Ninja')
        # Current dir is "test_package/build/<build_id>" and CMakeLists.txt is
        # in "test_package"
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")

    def test(self):
        os.chdir("bin")
        self.run(".%sexample" % os.sep)