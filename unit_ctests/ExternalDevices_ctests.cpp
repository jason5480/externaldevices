#include "catch2/catch.hpp"
#include "ExternalDevices/ExecutableDevice.hpp"
#include "ExternalDevices/MeasurableDevice.hpp"
#include <sstream>

TEST_CASE("Executable output", "[ExternalDevices_tests.cpp]")
{
	ext_executable_device executable;

	std::ostringstream out;
	executable.execute(out);
	std::string intended = "Executable device triggered";
	REQUIRE(out.str() == intended);
}

TEST_CASE("Merasureable output", "[ExternalDevices_tests.cpp]")
{
	ext_measurable_device measurable;

	std::ostringstream out;
	measurable.measure(out);
	std::string intended = "Measurable device triggered";
	REQUIRE(out.str() == intended);
}