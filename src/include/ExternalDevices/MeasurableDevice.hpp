/*
 * GOAL: Create simple sensor that can measure
 */

#pragma once
//#include "ExternalDevices_export.h"
#include <iostream>

/// The Executable Sensor
class ext_measurable_device // EXTERNALDEVICES_EXPORT
{
public:
	void measure(std::ostream& out) const
	{
		out << "Measurable device triggered";
	}
};