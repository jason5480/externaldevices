/*
 * GOAL: Create simple sensor that can be executed
 */

#pragma once
//#include "ExternalDevices_export.h"
#include <iostream>

/// The Executable Sensor
class ext_executable_device // EXTERNALDEVICES_EXPORT
{
public:
	void execute(std::ostream& out) const
	{
		out << "Executable device triggered";
	}
};